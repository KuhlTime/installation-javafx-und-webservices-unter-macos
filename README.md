# JavaFX und WebServices unter Java 14 und Intellij IDEA

**André Kuhlmann**

Dies ist eine kurze Anleitung für mich und jeden anderen der vor dem Problem stehen sollte JavaFX und WebServices auf macOS in Intellij IDEA ans laufen zu bringen.

Da JavaFX standardmäßig seid einiger Zeit nicht mehr mit Java mit geliefert wird stellte sich das ausführen des Praktikums als eine nervige Hürde heraus. Nach einiger Zeit im WWW habe ich es dann dennoch geschafft, in wenigen Schritten Java ans laufen zu bringen.

Die Schritte haben bei mir Funktioniert. Eventuell gibt es bessere Wege beide Frameworks ans laufen zu bekommen. 

----
### Schritt 1 - Java 14 installieren
Es kann zwischen der Oracle und der Adopt-OpenJDK gewählt werden. Letztere kann über Variante 1 und 2 installiert werden.
[Difference between OpenJDK and AdoptOpenJDK](https://stackoverflow.com/questions/52431764/difference-between-openjdk-and-adoptium-adoptopenjdk)

#### Variante 1 - In Intellij IDEA
Beim erstellen eines Projekts kann bei der Auswahl der Java Version automatisch die neuste openJDK Version installiert werden. (Sowohl Oracle als AdoptOpenJDK stehen zur Verfügung)
![](resources/Bildschirmfoto%202020-06-26%20um%2015.16.28.png)
![](resources/Bildschirmfoto%202020-06-26%20um%2015.16.00.png)

Hat man bereits ein Projekt erstellt kann die benötigte Java Version auch noch später in den Projekteinstellungen heruntergeladen und ausgewählt werden.

#### Variante 2 - Paketverwaltung
Wer bereits [Homebrew](https://brew.sh) installiert hat der kann die benötigte openJDK Version mit einem der folgenden Befehle installieren.

**Oracle**
```bash
brew cask install homebrew/cask-versions/java14
```

**AdoptOpenJDK**
```bash
brew cask install AdoptOpenJDK/openjdk/adoptopenjdk14
```
Mehr Infos unter: [https://github.com/AdoptOpenJDK/homebrew-openjdk](https://github.com/AdoptOpenJDK/homebrew-openjdk)

----
### Schritt 2 - JavaFX 14 installieren
Die Open Source Implementierung von JavaFX für macOS ist unter folgender Adresse zu finden. 
[https://gluonhq.com/products/javafx/](https://gluonhq.com/products/javafx/)
Das heruntergeladene Verzeichnis kann man sich auf dem Rechner in ein wieder auffindbares Verzeichnis packen.
```bash
/Users/<USERNAME>/.javafx/
```

Um sämtliche CLI Tools von javaFX verwenden zu können empfiehlt es sich eine Umgebungsvariable anzulegen.

```bash
export PATH_TO_FX="/Users/<USERNAME>/.javafx/javafx-sdk-14.0.1"
```

Ist dies getan muss die Bibliothek in das Projekt eingebunden werden. Dafür ist das öffnen der Projekteinstellungen nötig. Unter `Modules > Dependencies` kann mit dem `+` Button und einem Klick auf `Library > Java` das zuvor heruntergeladene Verzeichnis eingebunden werden.
```bash
/Users/<USERNAME>/.javafx/javafx-sdk-14.0.1/bin
```

Als letzter Schritt für JavaFX müssen folgende Flaggen in das Feld `VM options` in der `Runtime Configuration` eingetragen werden:
```bash
--add-modules javafx.controls,javafx.fxml
--module-path "/Users/<USERNAME>/.javafx/javafx-sdk-14.0.1/bin"
```

----
### Schritt 3 - WebService installieren
Auch die WebServices werden seid Java 11 nicht mehr mitgeliefert und benötigen zusätzliche Schritte um es ans laufen zu bringen.

Die Installation funktioniert über Maven, die Java Paketverwaltung.  
Ist man in den Projekteinstellungen unter Intellij kann unter `Modules > Dependencies` auf dem kleinen `+` eine Abhängigkeit hinzugefügt werden. Diesmal allerdings nicht als lokales Java Packet sonder über Maven.
![](resources/Bildschirmfoto%202020-06-26%20um%2015.14.21.png)

Das gewünschte Paket ist unter `com.sun.xml.ws:jaxws-rt:<VERSION>` zu finden.

Siehe auch:  
[https://stackoverflow.com/questions/54516361/how-to-use-webservices-on-java-11-package-javax-jws-does-not-exist](https://stackoverflow.com/questions/54516361/how-to-use-webservices-on-java-11-package-javax-jws-does-not-exist)

### Final
Das Projekt sollte dann etwa wie folgt aussehen:
![](resources/Bildschirmfoto%202020-06-26%20um%2015.12.17.png)